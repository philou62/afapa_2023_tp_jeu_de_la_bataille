package La_bataille;

import java.util.Arrays;
import java.util.List;

public class Carte {

	private final Rang rang;
	private final Couleur couleur;
	
	public Carte(Rang rang, Couleur couleur) {
		this.rang = rang;
		this.couleur = couleur;
	}

	public Rang getRang() {
		return rang;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	@Override
	public String toString() {
		return  rang + " de " + couleur ;
	}

	
	
	

}
