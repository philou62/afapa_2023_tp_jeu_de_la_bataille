package La_bataille;

import java.util.*;

public class Paquet {
	
	private static Carte[] cartes = new Carte[52];
//	List<Carte> cartes = new ArrayList<>();
	 
	public Paquet() {
		int i = 0;
		for (Couleur couleur : Couleur.values()) {
			for (Rang rang : Rang.values()) {
				cartes[i++] = new Carte(rang, couleur);
			}
		}
		melangerCartes(cartes);
		for (Carte carte : cartes) {
			System.out.println(carte);
		}
	}
//	public void imprimEtat() {
//		for (Carte carte : cartes) {
//			System.out.println(carte.toString());
//		}
//	}
	
	
	
	
	
	public void melangerCartes(Carte[] cartes) {
        Random rand = new Random();

        for (int i = cartes.length - 1; i > 0; i--) {
            // Générer un index aléatoire entre 0 et i inclus
            int indexAleatoire = rand.nextInt(i + 1);

            // Échanger l'élément courant avec l'élément à l'index aléatoire
            Carte temp = cartes[i];
            cartes[i] = cartes[indexAleatoire];
            cartes[indexAleatoire] = temp;
        }
    }
}
